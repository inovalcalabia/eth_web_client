import React from 'react';
import { Provider } from 'react-redux';
import { render } from 'react-dom';
import { compose, createStore, applyMiddleware } from 'redux';
import reduxThunk from 'redux-thunk'
import store from './store'
import App from './Components/App';
import './style.css';
//112223333333
const stores = createStore(store, applyMiddleware(reduxThunk));

render(
  <Provider store={stores}>
    <App />
  </Provider>,
  document.getElementById('root')
);
