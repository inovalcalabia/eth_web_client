import React, { Component } from 'react';
import { connect } from 'react-redux';
import { default as Web3} from 'web3';
import $ from 'jquery';
import Button from '@material-ui/core/Button';
import { TweenMax, Linear } from 'gsap';
class Game extends Component {
    constructor(props) {
        super(props);
        const WS_PROTOCOL = 'ws';
        const WS_HOST_NAME = IP_ADDRESS;
        const WS_TEST_PORT = '8082';
        const WS_PORT = '8082';
        const WS_END_POINT = '';
        this.state = {
            bets: [0, 0, 0, 0, 0 ,0],
            ETHbalance: 0,
            isLock: true,
            betIndex: 6,
            winningNumber: 1,
            currentTime: 0,
            winningPrice: 0
        };
        this.webSocket   = null;
        this.ws_protocol = null;
        this.ws_hostname = null;
        this.ws_port     = null;
        this.ws_endpoint = null;
        this.eth_address = 0;
        this.server_address = 0;
        this.betPrice = 0.00001;
        this.originalBet = 0;
        this.isGameNew = true;
        this.testMode = true;
        // // console.log("Running App version " + IP_ADDRESS);
       if (typeof web3 !== 'undefined') {
            window.web3 = new Web3(web3.currentProvider);
            web3.eth.getAccounts((function(err, accounts){
              if (err != null) {
                // console.error("An error occurred: "+err);
              }
              else if (accounts.length == 0) {
                // console.log("User is not logged in to MetaMask");
                alert('please login to your metamask account')
                ethereum.enable().then(()=>{
                    window.location.reload();
                }).catch((e)=>{
                    // console.log(e)
                    alert('User denied account authorization')
                });
              } else {
                // console.log("User is logged in to MetaMask", accounts);
                this.eth_address =  accounts[0];
                this.getETHBalance(() => {
                    let p = new URL(location.href).searchParams.get("port");
                    let netword_id = web3.currentProvider.networkVersion;
                    this.openWSConnection(WS_PROTOCOL, WS_HOST_NAME, WS_PORT, WS_END_POINT);
                    if (netword_id !== "1" && netword_id !== "2" && netword_id !== "3" && netword_id !== "4") {
                        document.getElementById("transaction-hash-cont-message").value = "no transaction history on localhost network";
                    } else {
                        document.getElementById("transaction-hash-cont-message").value = "transaction history";
                    }
                });


                ethereum.on('accountsChanged', function (accounts) {
                  // Time to reload your interface with accounts[0]!
                window.location.reload();
                })
              }
            }).bind(this));
       } else {
        alert('please install metamask, then reload the page')
       }
     
    }
    showWinningNumber(w, cb) {
        var ti = {t : 1}
        TweenMax.to(ti, 0.5, { 
          t: 6, 
          onUpdate: () => this.setState({winningNumber:  Math.floor(ti.t)}),
          onComplete: () =>  {this.setState({winningNumber:  w});if(cb){cb()}},
          repeat: 10,
          ease: Linear.easeNone,
        });
    }
    getETHBalance(callback) {
       web3.eth.getBalance(this.eth_address, ((err, balance) => {
          // balance = web3.utils.fromWei(balance, "ether") + " ETH " + balance
         this.setState({
            ETHbalance: web3.fromWei(balance, "ether")
         });
         this.originalETHBalance = balance;
         if (callback) {
            callback();
         }
      }).bind(this));
    }
    onConnectClick() {
        this.openWSConnection(this.ws_protocol, this.ws_hostname, this.ws_port, this.ws_endpoint);
    }
    onDisconnectClick() {
        this.webSocket.close();
    }
    openWSConnection(protocol, hostname, port, endpoint) {
        var webSocketURL = null;
        webSocketURL = protocol + "://" + hostname + ":" + port + endpoint;
        // console.log("openWSConnection::Connecting to: " + webSocketURL);

        try {
            this.webSocket = new WebSocket(webSocketURL);
            this.webSocket.onopen = this.onOpen.bind(this);
            this.webSocket.onclose = this.onClose.bind(this);
            this.webSocket.onerror = this.onError.bind(this);
            this.webSocket.onmessage = this.onMessage.bind(this);
            

        } catch (exception) {
            // console.error(exception);
        }
    }
    onOpen(openEvent) {
        // console.log("WebSocket OPEN: " + JSON.stringify(openEvent, null, 4));
        this.webSocket.send(JSON.stringify({ id: C_PLAYER_INFO, address: this.eth_address, network_id: web3.currentProvider.networkVersion}));
    }
    onMessage(messageEvent) {
        var wsMsg = JSON.parse(messageEvent.data);

        if (wsMsg.id === S_GAME_START) {
            // console.log("GAME_START", wsMsg);
            this.server_address = wsMsg.server_address;
        } else if (wsMsg.id === S_GAME_TIME) {
            // console.log("GAME_TIME", wsMsg);
            if (this.isGameNew || wsMsg.time === 40) {
                this.isGameNew = false;
                this.setState({isLock: wsMsg.isLock});
            }
            this.setState({ currentTime: wsMsg.time});
           //  document.getElementById("time").innerHTML = 'time: ' + wsMsg.time;
        } else if (wsMsg.id === S_TRANSACTION_HASH) {
             document.getElementById("transaction-hash-cont-message").value = "t-hash: " + wsMsg.url +"\r\n" + document.getElementById("transaction-hash-cont-message").value;
        } else if (wsMsg.id === S_GAME_END) {
            // console.log("GAME_END", wsMsg);
            this.isGameNew = true;
            setTimeout(() => {
                this.showWinningNumber(parseFloat(wsMsg.winning_number + 1), () => {
                    this.setState({
                      winningPrice: wsMsg.won_price
                    });
                });
            }, 1000);
            setTimeout(() => {
                this.getETHBalance(() => {
                    this.setState({
                      bets: [0, 0, 0, 0, 0, 0],
                      winningPrice: 0,
                      winningNumber: 0
                    });
                });
            }, 12000);

        } else if (wsMsg.id === GAME_HEART_BEAT) {
            // console.log("GAME_HEART_BEAT", wsMsg);
        }
    }
    onClose(closeEvent) {
        // console.log("WebSocket CLOSE: " + JSON.stringify(closeEvent, null, 4));
    }
    onError(errorEvent) {
         // console.log("WebSocket ERROR: " + JSON.stringify(errorEvent, null, 4));
         alert(' cannot connect to server, please try again');
    }
    onSendClick() {
        if (this.webSocket.readyState != this.WebSocket.OPEN) {
            // console.error("webSocket is not open: " + this.webSocket.readyState);
            return;
        }

        
    }
    placeBet(e) {
        if (this.betPrice > this.state.ETHbalance) {
            alert('not enough coins');
            return;
        }
        if (!this.state.isLock) {
            let index = parseFloat(e.currentTarget.getAttribute('index')) - 1;
            // console.log('this.betPrice', this.betPrice)
            let currentVal =  parseFloat(e.currentTarget.getAttribute('data-bet-val')) + this.betPrice;
            this.state.bets[index] = parseFloat(currentVal.toFixed(5));
            this.setState({
              bets: this.state.bets,
              ETHbalance: this.state.ETHbalance -  this.betPrice
            });
        }
        
    }
    selectBet(e) {
        if (!this.state.isLock) {
            this.betPrice = parseFloat(e.currentTarget.getAttribute('price'));
        }
        this.setState({
          betIndex: parseFloat(e.currentTarget.getAttribute('index'))
        });
    }
    sendBet() {
       if (!this.state.isLock) {
           this.sendETHCoin(this.getAllTotalBet(), (transactionHash) => {
            this.webSocket.send(JSON.stringify({ id: C_BET, bets: this.state.bets, totalBet: this.getAllTotalBet()}));
            this.setState({isLock: true});;
           });
        }
    }

    sendETHCoin(bet, callback) {
        web3.eth.sendTransaction({
            from: this.eth_address,
            to: this.server_address, 
            value: web3.toWei(String(bet), "ether"), 
        }, function(err, transactionHash) {
            if (err) { 
                // console.log(err); 
            } else {
                if (callback) {
                    callback(transactionHash);
                }
            }
        });
    }
    getAllTotalBet() {
        let total = 0;
        for (let i = 0; i < this.state.bets.length; i += 1) {
            total += parseFloat(this.state.bets[i]);
        }
        return parseFloat(total.toFixed(4));
    }
    clearBet() {
        this.setState({
          ETHbalance:this.state.ETHbalance + this.getAllTotalBet(),
          bets: [0, 0, 0, 0, 0, 0],
        });
    }
    render() {
        return (
          <div id='appWrapper'>
            <h1 id='game-title'> Guess the number </h1>
            
            <div id="player-info-wrapper">
            </div>
            <div className="grid-container">
              <div className="grid-item">
                <div className="grid-container-sm">
                  <div className="grid-item-sm">
                    <div>ETH Balance: {parseFloat(this.state.ETHbalance).toFixed(10)}</div>
                  </div>
                  <div className="grid-item-sm time">
                    <div className="time">Time: {this.state.currentTime} </div>
                  </div>
                  <div className="grid-item-sm game-status">
                    <div className="game-status">Status: { this.state.isLock ? <i className="fa fa-lock"></i> : <i className="fa fa-unlock"></i>} </div>
                  </div>
                </div>
              </div>
              <div className="grid-item">
                <div className="grid-container-sm">
                  <div className="grid-item-sm">
                    <div>winning number</div>
                  </div>
                  <div className="grid-item-sm winning">
                    <div className="winning">{this.state.winningNumber}</div>
                  </div>
                  <div className="grid-item-sm">
                    <div>winning price</div>
                  </div>
                  <div className="grid-item-sm winning">
                    <div className="winning">{this.state.winningPrice}</div>
                  </div>
                </div>
              </div>
            </div>
            <div id="bet-table-wrapper">
                <div className='bet-table-cont '>
                    <span className='bet-label'> 1 </span>
                    <Button disabled={this.state.isLock} index='1' className="noselect bet-table" data-bet-val = {this.state.bets[0]} onClick={this.placeBet.bind(this)}>{this.state.bets[0]}

                    </Button>

                </div>
               
                 <div className='bet-table-cont'>
                    <span className='bet-label'> 2 </span> 
                    <Button disabled={this.state.isLock} index='2' className="noselect bet-table" data-bet-val = {this.state.bets[1]} onClick={this.placeBet.bind(this)}>{this.state.bets[1]}</Button>
                </div>

                 <div className='bet-table-cont'>
                    <span className='bet-label'> 3 </span> 
                    <Button disabled={this.state.isLock} index='3' className="noselect bet-table" data-bet-val = {this.state.bets[2]} onClick={this.placeBet.bind(this)}>{this.state.bets[2]}</Button>
                </div>

                 <div className='bet-table-cont'>
                    <span className='bet-label'> 4 </span> 
                    <Button disabled={this.state.isLock} index='4' className="noselect bet-table" data-bet-val = {this.state.bets[3]} onClick={this.placeBet.bind(this)}>{this.state.bets[3]}</Button>
                </div>

                 <div className='bet-table-cont'>
                    <span className='bet-label'> 5 </span> 
                    <Button disabled={this.state.isLock} index='5' className="noselect bet-table" data-bet-val = {this.state.bets[4]} onClick={this.placeBet.bind(this)}>{this.state.bets[4]}</Button>
                </div>

                 <div className='bet-table-cont'>
                    <span className='bet-label'> 6 </span> 
                    <Button disabled={this.state.isLock} index='6' className="noselect bet-table" data-bet-val = {this.state.bets[5]} onClick={this.placeBet.bind(this)}>{this.state.bets[5]}</Button>
                </div>
            </div>

            
            <div id="bet-amount-wrapper">
                <Button disabled={this.state.isLock} index = '1' color={(this.state.betIndex === 1) ? "primary" : null} variant="contained" price='1' onClick={this.selectBet.bind(this)}> 1 </Button>
                <Button disabled={this.state.isLock} index = '2' color={(this.state.betIndex === 2) ? "primary" : null} variant="contained" price='0.1' onClick={this.selectBet.bind(this)}> 0.1 </Button>
                <Button disabled={this.state.isLock} index = '3' color={(this.state.betIndex === 3) ? "primary" : null} variant="contained" price='0.01' onClick={this.selectBet.bind(this)}> 0.01 </Button>
                <Button disabled={this.state.isLock} index = '4' color={(this.state.betIndex === 4) ? "primary" : null} variant="contained" price='0.001' onClick={this.selectBet.bind(this)}> 0.001 </Button>
                <Button disabled={this.state.isLock} index = '5' color={(this.state.betIndex === 5) ? "primary" : null}variant="contained" price='0.0001' onClick={this.selectBet.bind(this)}> 0.0001 </Button>
                <Button disabled={this.state.isLock} index = '6' color={(this.state.betIndex === 6) ? "primary" : null}variant="contained" price='0.00001' onClick={this.selectBet.bind(this)}> 0.00001 </Button>
                <Button disabled={this.state.isLock} variant="contained" onClick={this.sendBet.bind(this)}> bet now  </Button>
                <Button disabled={this.state.isLock} variant="contained" onClick={this.clearBet.bind(this)}> clear bet  </Button>

            </div>
            <textarea id="transaction-hash-cont-message" readOnly>

            </textarea>
          </div>
        );
      }
    }

function mapStateToProps(state) {
  return {
  }
}
const mapDispatchToProps = {
}
const S_GAME_START = 50;
const S_GAME_TIME = 100;
const S_GAME_END = 200;
const S_TRANSACTION_HASH = 250;
const C_PLAYER_INFO = 300;
const C_BET = 400;

const GAME_HEART_BEAT = 500;

export default connect(mapStateToProps, mapDispatchToProps)(Game);
