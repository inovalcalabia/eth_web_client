# Simple Crypto Game

## How to play
- select bet value
- place the bet on the table numbers that range from 1 - 6
- game period 40 seconds
- lock game period 20 seconds

## Notes

```sh
- please install metamask chrome plugin
- please use git bash or powershell if npm install is not a success on installing on cmd (node-gyp(web3) issue)
```


## How to run

```sh
git clone https://inovalcalabia@bitbucket.org/inovalcalabia/eth_web_client.git
npm install
npm run start || npm run build
```

## Test locally

```sh
npm install  -g ganache-cli
ganache-cli -m "metamask seed pharse"
cd eth_web_client
npm install
npm run start
```

## Author

👤 **inovalcalabia**

* Twitter: [@inovalcalabia](https://twitter.com/inovalcalabia)
* Github: [@inovalcalabia](https://github.com/inovalcalabia)
